# Bitbucket Pipelines Examples
Find and share bitbucket-pipelines.yml examples and scripts. Learn more about [Bitbucket Pipelines Beta](https://bitbucket.org/product/features/pipelines).

## Examples
- [Java-Maven](/examples/java-maven)
- [Java-Gradle](/examples/java-gradle)
- [Node.js](/examples/nodejs)
- [Ruby](/examples/ruby)
- [Python](/examples/python)
- [Haskell-Stack](/examples/haskell-stack)
- [PHP](/examples/php)

## Share your example
1. Add a directory with:
    - Example bitbucket-pipelines.yml
    - Additional scripts (if required)
    - README.md (if required)
2. Add a link in the root README.md to the directory of your example

## Resources
- [Bitbucket Pipelines Documentation](https://confluence.atlassian.com/pages/viewpage.action?pageId=792496469)
- [Atlassian Answers](https://answers.atlassian.com/questions/topics/38241650/bitbucket-pipelines)
- [Suggest a feature](https://bitbucket.org/site/master/issues?status=new&status=open&component=Pipelines)
- [Report a bug](https://support.atlassian.com/servicedesk/customer/portal/11/create/119)